package boletín.pkg18;

import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showInputDialog;

public class Boletín18 {
    
    public static void main(String[] args) {
        
        String[] nomeEquipos = new String[5];
        String[] xornadas = new String[4];
        int[][] goles = new int[5][4];
        
        for (int i = 0; i < xornadas.length; i++) {
            
            xornadas[i] = JOptionPane.showInputDialog(" Xornadas: " + (i + 1));
            
        }
        
        for (int i = 0; i < nomeEquipos.length; i++) {
            
            nomeEquipos[i] = JOptionPane.showInputDialog(" Nombre del Equipo:" + (i + 1));
            
            for (int j = 0; j < goles[i].length; j++) {
                
                goles[i][j] = Integer.parseInt(showInputDialog(" Goles: " + (j + 1)));
                
            }
            
            
        }
       
        Futbol18.visualizar(nomeEquipos, xornadas, goles);
        
    }
    
}
