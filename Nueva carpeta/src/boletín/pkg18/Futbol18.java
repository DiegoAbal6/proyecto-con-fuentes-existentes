package boletín.pkg18;

public class Futbol18 {

    public static void visualizar(String[] nomeEquipos, String[] xornadas, int[][] goles) {

        for (int i = 0; i < nomeEquipos.length; i++) {
                System.out.printf("\n" +"%-15s", nomeEquipos[i] + "    ");
            for (int j = 0; j < xornadas.length; j++) {
                System.out.print("    " + goles[i][j]);
            }      
        }
    }
}